package com.michalrys.indexedwords.datacontainers;

import org.junit.Assert;
import org.junit.Test;

public class UniqueLettersFactoryTest {

    @Test
    public void shouldGetUniqueLettersContainerForGivenWord() {
        //given
        String givenWord = "alamakota";
        LetterContainerFactory lettersFactory = new UniqueLettersFactory();

        //when
        LetterContainer letterContainer = lettersFactory.create(givenWord);

        //then
        Assert.assertTrue(letterContainer instanceof UniqueLetters);
    }
}