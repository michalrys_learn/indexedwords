package com.michalrys.indexedwords.datacontainers;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;

public class WordsUniqueAndNoCaseSensitivityTest {

    @Test
    public void shouldGetListOfWordsForSimpleTextWhereWordsAreSeparatedBySpace() {
        //given
        String inputText = "ala ma kota";
        List<String> expectedResult = Arrays.asList(
                "ala",
                "ma",
                "kota"
        );
        WordContainer words = new WordsUniqueAndNoCaseSensitivity(inputText);

        //when
        List<String> result = words.get();

        //then
        Assert.assertEquals(new TreeSet<>(expectedResult), new TreeSet<>(result));
    }

    @Test
    public void shouldGetListOfWordsForSimpleTextWhereWordsAreSeparatedBySpaceAndAreDuplicated() {
        //given
        String inputText = "ala ma kota kota kota";
        List<String> expectedResult = Arrays.asList(
                "ala",
                "ma",
                "kota"
        );
        WordContainer words = new WordsUniqueAndNoCaseSensitivity(inputText);

        //when
        List<String> result = words.get();

        //then
        Assert.assertEquals(new TreeSet<>(expectedResult), new TreeSet<>(result));
    }

    @Test
    public void shouldGetListOfWordsForTextWhereWordsAreSeparatedByDifferentCharacter() {
        //given
        String inputText = "ala,ala, ola i pies - kot (bury,mały) jest. a co z... tomkiem? też! leci f-16, " +
                "z h2o intel33#2. obejrzał film człowiek-pająk 2. -testa testb-";
        List<String> expectedResult = Arrays.asList(
                "ala",
                "ola",
                "i",
                "pies",
                "kot",
                "bury",
                "mały",
                "jest",
                "a",
                "co",
                "z",
                "tomkiem",
                "też",
                "leci",
                "z",
                "obejrzał",
                "film",
                "człowiek-pająk",
                "testa",
                "testb"
        );
        //TODO: ask client about word definition: here I assumed :
        //TODO: 1) word cannot have other characters than letters, so f-16, h2o, intel33#2 are not words.
        //TODO: 2) two words connected by '-' are treated as a single word, so człowiek-pająk is a single word.
        //TODO: 3) word which starting or ending with '-', '-' is treated as separator, so -ty or ty- is a word ty.
        WordContainer words = new WordsUniqueAndNoCaseSensitivity(inputText);

        //when
        List<String> result = words.get();

        //then
        Assert.assertEquals(new TreeSet<>(expectedResult), new TreeSet<>(result));
    }

    @Test
    public void shouldGetListOfWordsLowerCaseOnly() {
        //given
        String inputText = "Ala i kOt - KOT i ALa... aLa";
        List<String> expectedResult = Arrays.asList(
                "ala",
                "kot",
                "i"
        );
        WordContainer words = new WordsUniqueAndNoCaseSensitivity(inputText);

        //when
        List<String> result = words.get();

        //then
        Assert.assertEquals(new TreeSet<>(expectedResult), new TreeSet<>(result));
    }
}