package com.michalrys.indexedwords.datacontainers;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class UniqueLettersTest {

    @Test
    public void shouldGetUniqueSetOfLettersFromExampleWord1() {
        //given
        String exampleWords = "ala";
        Set<Character> expectedResult = new HashSet<>();
        expectedResult.addAll(Arrays.asList('a', 'l'));
        LetterContainer uniqueLetters = new UniqueLetters(exampleWords);

        //when
        Set<Character> result = uniqueLetters.get();

        //then
        Assert.assertEquals(new TreeSet<>(expectedResult), new TreeSet<>(result));
    }

    @Test
    public void shouldGetUniqueSetOfLettersFromListOfWordsExample2() {
        //given
        String exampleWords = "koduje kod";
        Set<Character> expectedResult = new HashSet<>();
        expectedResult.addAll(Arrays.asList('k', 'o', 'd', 'u', 'j', 'e', ' '));
        LetterContainer uniqueLetters = new UniqueLetters(exampleWords);

        //when
        Set<Character> result = uniqueLetters.get();

        //then
        Assert.assertEquals(new TreeSet<>(expectedResult), new TreeSet<>(result));
    }
}