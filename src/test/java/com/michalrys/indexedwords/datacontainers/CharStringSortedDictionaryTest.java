package com.michalrys.indexedwords.datacontainers;

import org.junit.Assert;
import org.junit.Test;

public class CharStringSortedDictionaryTest {

    @Test
    public void shouldGetIndexedWordsForGivenWordsExample1() {
        //given
        String wordsExample = "ala ma kota kot koduje";
        String expectedResults = "" +
                "a : ala, kota, ma\n" +
                "d : koduje\n" +
                "e : koduje\n" +
                "j : koduje\n" +
                "k : koduje, kot, kota\n" +
                "l : ala\n" +
                "m : ma\n" +
                "o : koduje, kot, kota\n" +
                "t : kot, kota\n" +
                "u : koduje";
        LetterContainerFactory lettersFactory = new UniqueLettersFactory();
        WordContainer words = new WordsUniqueAndNoCaseSensitivity(wordsExample);
        DictionaryContainer dictionary = new CharStringSortedDictionary(lettersFactory, words);

        //when
        String outputResult = dictionary.getOutput();

        //then
        Assert.assertEquals(expectedResults, outputResult);
    }

    @Test
    public void shouldGetIndexedWordsForGivenWordsExample2() {
        //given
        String wordsExample = "ala ma kota kot koduje w javie";
        String expectedResults = "" +
                "a : ala, javie, kota, ma\n" +
                "d : koduje\n" +
                "e : javie, koduje\n" +
                "i : javie\n" +
                "j : javie, koduje\n" +
                "k : koduje, kot, kota\n" +
                "l : ala\n" +
                "m : ma\n" +
                "o : koduje, kot, kota\n" +
                "t : kot, kota\n" +
                "u : koduje\n" +
                "v : javie\n" +
                "w : w";
        LetterContainerFactory lettersFactory = new UniqueLettersFactory();
        WordContainer words = new WordsUniqueAndNoCaseSensitivity(wordsExample);
        DictionaryContainer dictionary = new CharStringSortedDictionary(lettersFactory, words);

        //when
        String outputResult = dictionary.getOutput();

        //then
        Assert.assertEquals(expectedResults, outputResult);
    }
}