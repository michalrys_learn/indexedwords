package com.michalrys.indexedwords;

import org.junit.Assert;
import org.junit.Test;

public class IndexedWordsTest {

    @Test
    public void shouldGetIndexedWordsForSingleSimpleWord() {
        //given
        String simpleExample = "momo";
        String expectedResults = "m : momo\no : momo";
        TransformedText indexedWords = new IndexedWords(simpleExample);

        //when
        String indexedWordsResult = indexedWords.get();

        //then
        Assert.assertEquals(expectedResults, indexedWordsResult);
    }

    @Test
    public void shouldGetIndexedWordsForSecondSingleSimpleWord() {
        //given
        String simpleExample = "yaayaa";
        String expectedResults = "a : yaayaa\ny : yaayaa";
        TransformedText indexedWords = new IndexedWords(simpleExample);

        //when
        String indexedWordsResult = indexedWords.get();

        //then
        Assert.assertEquals(expectedResults, indexedWordsResult);
    }

    @Test
    public void shouldGetIndexedWordsForSimplePairOfWordsSeparatedBySpace() {
        //given
        String simpleExample = "no an";
        String expectedResults = "a : an\nn : an, no\no : no";
        TransformedText indexedWords = new IndexedWords(simpleExample);

        //when
        String indexedWordsResult = indexedWords.get();

        //then
        Assert.assertEquals(expectedResults, indexedWordsResult);
    }

    @Test
    public void shouldGetIndexedWordsForLongTextWordsExampleSeparatedBySpace() {
        //given
        String simpleExample = "ala ma kota kot koduje";
        String expectedResults = "" +
                "a : ala, kota, ma\n" +
                "d : koduje\n" +
                "e : koduje\n" +
                "j : koduje\n" +
                "k : koduje, kot, kota\n" +
                "l : ala\n" +
                "m : ma\n" +
                "o : koduje, kot, kota\n" +
                "t : kot, kota\n" +
                "u : koduje";
        TransformedText indexedWords = new IndexedWords(simpleExample);

        //when
        String indexedWordsResult = indexedWords.get();

        //then
        Assert.assertEquals(expectedResults, indexedWordsResult);
    }
    @Test
    public void shouldGetIndexedWordsForTaskExampleText() {
        //given
        String simpleExample = "ala ma kota, kot koduje w Javie kota";
        String expectedResults = "" +
                "a : ala, javie, kota, ma\n" +
                "d : koduje\n" +
                "e : javie, koduje\n" +
                "i : javie\n" +
                "j : javie, koduje\n" +
                "k : koduje, kot, kota\n" +
                "l : ala\n" +
                "m : ma\n" +
                "o : koduje, kot, kota\n" +
                "t : kot, kota\n" +
                "u : koduje\n" +
                "v : javie\n" +
                "w : w";
        TransformedText indexedWords = new IndexedWords(simpleExample);

        //when
        String indexedWordsResult = indexedWords.get();

        //then
        Assert.assertEquals(expectedResults, indexedWordsResult);
    }
}