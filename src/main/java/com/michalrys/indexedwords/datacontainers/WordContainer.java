package com.michalrys.indexedwords.datacontainers;

import java.util.List;

public interface WordContainer {
    List<String> get();
}
