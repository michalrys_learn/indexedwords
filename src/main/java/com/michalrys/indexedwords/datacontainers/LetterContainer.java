package com.michalrys.indexedwords.datacontainers;

import java.util.Set;

public interface LetterContainer {
    Set<Character> get();
}
