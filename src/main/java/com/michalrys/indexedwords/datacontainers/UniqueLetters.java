package com.michalrys.indexedwords.datacontainers;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class UniqueLetters implements LetterContainer {
    private Set<Character> letters = new HashSet<>();

    public UniqueLetters(String word) {
        setLetters(word);
    }

    private void setLetters(String word) {
        Set<Character> lettersFromSingleWord = word.chars()
                .mapToObj(i -> (char) i)
                .collect(Collectors.toSet());
        letters.addAll(lettersFromSingleWord);
    }

    @Override
    public Set<Character> get() {
        return letters;
    }
}