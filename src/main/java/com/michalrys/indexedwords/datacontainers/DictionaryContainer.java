package com.michalrys.indexedwords.datacontainers;

public interface DictionaryContainer {
    String getOutput();
}
