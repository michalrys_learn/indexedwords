package com.michalrys.indexedwords.datacontainers;

import java.util.*;
import java.util.stream.Collectors;

public class WordsUniqueAndNoCaseSensitivity implements WordContainer {
    private static final String WORD_SEPARATORS = " ,.:;()!?";
    private List<String> words = new ArrayList<>();

    public WordsUniqueAndNoCaseSensitivity(String inputText) {
        setWords(inputText);
    }

    private void setWords(String inputText) {
        String wordsLowerCased = inputText.toLowerCase();
        String[] wordsSplitted = wordsLowerCased.split("[" + WORD_SEPARATORS + "]+");
        Set<String> wordsUnique = new HashSet<>(Arrays.asList(wordsSplitted));
        Set<String> wordsOnly = wordsUnique.stream()
                .map(s -> s.replaceAll(".*[^a-zęóąśłżźćń-]+.*", ""))
                .map(s -> s.replaceAll("(^-+)|(-+$)", ""))
                .filter(s -> s.length() > 0)
                .collect(Collectors.toSet());
        words.addAll(wordsOnly);
    }

    @Override
    public List<String> get() {
        return words;
    }
}