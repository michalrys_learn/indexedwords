package com.michalrys.indexedwords.datacontainers;

public class UniqueLettersFactory implements LetterContainerFactory {
    @Override
    public LetterContainer create(String word) {
        return new UniqueLetters(word);
    }
}