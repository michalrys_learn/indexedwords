package com.michalrys.indexedwords.datacontainers;

import java.util.*;

public class CharStringSortedDictionary implements DictionaryContainer {
    private LetterContainerFactory lettersFactory;
    private WordContainer words;
    private Map<Character, Set<String>> indexedWords = new TreeMap<>();
    private String output;

    public CharStringSortedDictionary(LetterContainerFactory lettersFactory, WordContainer words) {
        this.lettersFactory = lettersFactory;
        this.words = words;
        setIndexedWords();
        setOutput();
    }

    private void setIndexedWords() {
        for (String word : words.get()) {
            LetterContainer letters = lettersFactory.create(word);

            for (char letter : letters.get()) {
                Set<String> wordsToAdd = indexedWords.getOrDefault(letter, new TreeSet<>());
                wordsToAdd.add(word);
                indexedWords.put(letter, wordsToAdd);
            }
        }
    }

    private void setOutput() {
        StringBuffer outputBuilder = new StringBuffer();
        for (char letter : indexedWords.keySet()) {
            outputBuilder.append(letter + " : ");
            for (String word : indexedWords.get(letter)) {
                outputBuilder.append(word);
                outputBuilder.append(", ");
            }
            removeCharsFromEnd(outputBuilder, 2);
            outputBuilder.append("\n");
        }
        removeCharsFromEnd(outputBuilder, 1);
        output = outputBuilder.toString();
    }

    private void removeCharsFromEnd(StringBuffer text, int howManyLettersToRemoveFromEnd) {
        text.replace(text.length() - howManyLettersToRemoveFromEnd, text.length(), "");
    }

    @Override
    public String getOutput() {
        return output;
    }
}