package com.michalrys.indexedwords.datacontainers;

public interface LetterContainerFactory {
    LetterContainer create(String word);
}
