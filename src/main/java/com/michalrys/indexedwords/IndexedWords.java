package com.michalrys.indexedwords;

import com.michalrys.indexedwords.datacontainers.*;

public class IndexedWords implements TransformedText {
    private WordContainer words;
    private LetterContainerFactory lettersFactory;
    private DictionaryContainer indexedWords;

    public IndexedWords(String inputText) {
        words = new WordsUniqueAndNoCaseSensitivity(inputText);
        lettersFactory = new UniqueLettersFactory();
        indexedWords = new CharStringSortedDictionary(lettersFactory, words);
    }

    @Override
    public String get() {
        return indexedWords.getOutput();
    }
}