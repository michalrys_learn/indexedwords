package com.michalrys.indexedwords;

public interface TransformedText {
    String get();
}
